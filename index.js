const express = require("express");
const app = express();
const port = 8000;

app.get("/devcamp-pizza365/orders", (req, res) => {
    res.status(200).json({
        message: `Get all orders`
    });
});

app.get("/devcamp-pizza365/orders/:orderId", (req, res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Get orderId ${orderId}`
    });
});

app.post("/devcamp-pizza365/orders", (req, res) => {
    res.status(200).json({
        message: `Create orders`
    });
});

app.put("/devcamp-pizza365/orders/:orderId", (req, res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Update orderId ${orderId}`
    });
});

app.delete("/devcamp-pizza365/orders/:orderId", (req, res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Delete orderId ${orderId}`
    });
});

app.listen(port, () => {
    console.log("App listening on port: ", port);
});